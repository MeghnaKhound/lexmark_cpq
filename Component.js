sap.ui.core.UIComponent.extend("lexmark.cpq.poc.Component", {
	metadata: {
		name: "LexmarkPOC",
		version: "1.0.0",
		includes: ["styles/css/styles.css", "styles/css/common.css","styles/css/main.css","styles/css/font.css"],
		dependencies: {
			libs: ["sap.m"]
		},
		//rootView: "lexmark.cpq.poc.view.App"
	}
});
