jQuery.sap.require("lexmark.cpq.poc.view.common.notificationBar");
sap.ui.jsview("lexmark.cpq.poc.view.App", {

    getControllerName : function() {
        return "lexmark.cpq.poc.view.App";
    },

    createContent : function(oController) {
        lexmarkFonts.addKnownFontsToPool();
        var controls = [];
         // to avoid scrollbars on desktop the root view must be set to block display
        this.setDisplayBlock(true);

        this.app = new sap.m.SplitApp({
            afterDetailNavigate: function() {
                    this.hideMaster();
            }
        });

        this.app.addDetailPage(sap.ui.jsview("LandingPage", "view.launchpad.landingPage"));
        this.app.addDetailPage(sap.ui.xmlview("CreateQuote", "lexmark.cpq.poc.view.createQuoteComplete"));
        this.app.addDetailPage(sap.ui.jsview("ProductConfigure", "view.product-configure.productConfigure"));

        var header =  new sap.ui.jsview("lexmarkHeader", "view.common.header");
        controls.push(header);
        controls.push(this.app);
        controls.push(oNotiBar3);

        return controls;

    }

});