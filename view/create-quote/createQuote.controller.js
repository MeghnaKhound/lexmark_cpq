sap.ui.controller("view.create-quote.createQuote", {

	configureProduct:function(event){
		var productsTable = sap.ui.getCore().byId("productsTable");
		var selectedIndex = event.getSource().getParent().getIndex();
		productsTable.setSelectedIndex(selectedIndex);

		appConfig.currentProductIndex = selectedIndex;

		if(appConfig.quoteId === ""){
			var url = oDataServices.createQuote;
			busyIndicatorMsg.busySpinner("Product Configure","Configuring...");
			this.createQuote(url);
		}else{
			this.getProductCharacteristics();
		}
	},

	createQuote:function(url){
		var payload= this.generatePayload();
		//var url=oDataServices.createQuote;
		
		utilModel.makeGatewayServiceCreateCall(url,"/CreateQuotationReqSet", payload, this.createQuoteResponse, this);
	},

	generatePayload:function(){
		var quotationItems=[];
		for (var key in productsToBeAdded.products) {
			var productItems;
		   	if (productsToBeAdded.products.hasOwnProperty(key)) {
		      	var obj = productsToBeAdded.products[key];
		      	var productID;
		      	for (var prop in obj) {
		         	if (obj.hasOwnProperty(prop)) {
		            	console.log(prop + " = " + obj[prop]);
		            	if(prop=="PRODUCT_ID") productID= obj[prop];
		            	else if(prop=="QUANTITY") quantity= String(obj[prop]);
		         	}
		      	}
		      	productItems ={"ProductId"	: productID,
    							   "Quantity"    : quantity};
		   	}
		   	quotationItems.push(productItems);
		}

		var dateStr=sap.ui.getCore().byId("postingDate").getValue();
		var poDate=new Date(dateStr);
		var day,month,year;
		if(parseFloat(poDate.getDate())<10) day="0"+poDate.getDate();
		else day=poDate.getDate();
		if(parseFloat(poDate.getMonth())<10) month="0"+poDate.getMonth();
		else month=poDate.getMonth()+1;
		year=poDate.getFullYear();
		var requiredPoDate=""+year+day+month;
		var productObject ={
  			"NAV_QUOTATION_HEADER":  [{
  				"SoldToParty"	: sap.ui.getCore().byId("soldToParty").getValue(),
  				//"PostingDate"	: sap.ui.getCore().byId("postingDate").getValue(),
  				"PostingDate": requiredPoDate,
  				"Description"	: sap.ui.getCore().byId("description").getValue(),
  				"CustomerPoNum"	: sap.ui.getCore().byId("customerPoNum").getValue()
				}
			],
			"NAV_QUOTATION_ITEMS":  quotationItems
		};

		return productObject;
	},
	createQuoteResponse:function (result, status, controller) {  
		busyDialog.close();
		if(status){
			appConfig.quoteId = result.QuotationId;
			this.getProductCharacteristics();
			//notifier.showSuccessMessage(appConfig.quoteId+": Quote created successfully");
		}
		else{
			notifier.showErrorMessage("Create quote has failed.");
			setTimeout(function(){
			  notifier.hideNotifier();
			}, 10000);
			return;
		}
	},

	getProductCharacteristics:function(){
		var url = oDataServices.getCharacteristics;

		var relPath = "ProdCsticsReqSet(QuotationId='QUOTEID',ItemIndex='" + (appConfig.currentProductIndex +1) + "')/NAV_REQ_TO_RES/?$expand=NAV_HEADER_TO_ITEM_NAME/NAV_ITEM_NAME_TO_VALUE";
		relPath = relPath.replace('QUOTEID', appConfig.quoteId);
		busyIndicatorMsg.busySpinner("Configure Product","Configuring...");
		utilModel.makeGatewayServiceReadCall(url, relPath, '', this.getProductCharacteristicsResponse, this);

	},

	getProductCharacteristicsResponse:function(response, status, controller){
		busyDialog.close();
		if(status){
			var prodCharacteristicsModel = new sap.ui.model.json.JSONModel();
			prodCharacteristicsModel.setData(response);
			appConfig.sessionIds[response.InstanceId] = response.SessionId;
			/*var lModel = new sap.ui.model.json.JSONModel();
			sap.ui.getCore().setModel(lModel, "configuration");
			lModel.loadData('model/configure.json',null,false);*/

			var productConfigureView = sap.ui.getCore().byId("ProductConfigure");

			productConfigureView.setModel(prodCharacteristicsModel);

			//this.createDialogue(productConfigureView);			

			productConfigureView.getController().configureRootProduct();

			sap.ui.getCore().getEventBus().publish("nav", "to", {id : "ProductConfigure"});

		}
		else{
			notifier.showErrorMessage("Product characteristics fetching failed");
			setTimeout(function(){
			  notifier.hideNotifier();
			}, 10000);
			return;
		}
	},

	createDialogue:function(productConfigureView){
		
		var oBtnCancel = new sap.ui.commons.Button({
				text : "Cancel",
				press : function() {
					this.destroyContent();
				}

			});

		var oBtnSaveConfig = new sap.ui.commons.Button({
			text : "Save Configuration",
			press : function() {
				this.getController().saveConfiguration();
				this.destroyContent();
			}
		});

		var oDialog = new sap.ui.commons.Dialog({
						modal : true,
						showCloseButton :false,
						buttons : [ oBtnCancel, oBtnSaveConfig],
						content : [ productConfigureView ]
					});

		oDialog.open();
	},


	submitQuote:function(){
		var rootUrl = oDataServices.updateQuote;
		var sessionId = appConfig.sessionIds["101"] ;
		var relPath = "/CreateQuotationReqSet(QuotationId='"+ appConfig.quoteId + "',SessionId='" + sessionId + "')/NAV_QTE_DETAIL";
		busyIndicatorMsg.busySpinner("Submit Quote","Submitting...");
		utilModel.makeGatewayServiceReadCall(rootUrl,relPath, "", this.submitQuoteResponse, this);
	},

	submitQuoteResponse:function(response, status){
		busyDialog.close();
		if (status) {
			sap.ui.getCore().byId("QuotationId").setText(appConfig.quoteId);
			notifier.showSuccessMessage(appConfig.quoteId+": Quote submitted Successfully");
		}
		else {
			notifier.showErrorMessage("Quote updation has failed.");
			setTimeout(function(){
			  notifier.hideNotifier();
			}, 10000);
			return;
		}
	},

	quantityChangeHandler:function(event){
		var newQuantity = event.getParameter("liveValue").trim();
		if(newQuantity !== "" && !isNaN(newQuantity)){
			var unitCost = event.getSource().getParent().mAggregations.cells[4].getText();

			var lineTotal = parseFloat(newQuantity) * parseFloat(unitCost);
			event.getSource().getParent().mAggregations.cells[5].setText(String(lineTotal));
			var estimatedTotal = 0;

			var rows = event.getSource().getParent().getParent().getRows();
			for(var i=0; i< rows.length; i++){
				var row = rows[i];
				lineTotal = row.mAggregations.cells[5].getText();
				if(lineTotal === ""){
					break;
				}
					estimatedTotal += parseFloat(estimatedTotal) + parseFloat(lineTotal);
				
			}
			var formattedEstimatedTotal=estimatedTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
			sap.ui.getCore().byId("estimatedTotalValue").setText(formattedEstimatedTotal);
		}
		
	}
	
});
