sap.ui.jsview("view.create-quote.createQuote", {
	getControllerName : function() {
		return "view.create-quote.createQuote";
	},

	createContent : function(oController) {
		var controls = [];


		var oMatrix = new sap.ui.commons.layout.MatrixLayout("customerInfo",{layoutFixed: true, width: '300px', columns: 3});
						oMatrix.setWidths('250px', '250px', '250px');

		oMatrix.createRow(new  sap.m.Label({text: "Quotation ID"}).addStyleClass("fieldLabel"), 
						  new  sap.m.Label({text: "Description"}).addStyleClass("fieldLabel"), 
						  new  sap.m.Label({text: "Sold to Party"}).addStyleClass("fieldLabel"));

		var quotationId = new  sap.m.Label("QuotationId", {text: ""});

		var description = new   sap.m.Input("description", {value: "Test Quotation"}).addStyleClass("quoteDetailsFields");

		//var soldToParty = new   sap.m.Text("soldToParty", {text: "272213"});
		var soldToParty = new   sap.m.Input("soldToParty", {value: "272213"}).addStyleClass("quoteDetailsFields");

		oMatrix.createRow(quotationId, description, soldToParty);

		var someLabel = new   sap.m.Label( {text: ""});

		


		oMatrix.createRow(new  sap.m.Label({text: "PO Number"}).addStyleClass("fieldLabel"), 
						new  sap.m.Label({text: "Date of Issue"}).addStyleClass("fieldLabel"), 
						  new  sap.m.Label({text: ""}));

		//var customerPoNum = new  sap.m.Text("customerPoNum", {text: "123456"});
		var customerPoNum = new  sap.m.Input("customerPoNum", {value: "123456"}).addStyleClass("quoteDetailsFields");

		var mtitle = new sap.m.HBox({
					items:[
						new sap.m.Label({ text:"Customer Information"}).addStyleClass("custInfoText")
					]
						}).addStyleClass("customerInformationTitle");


		var postingDate =new  sap.m.DatePicker("postingDate",{
		value: "09/06/2015"
		}).addStyleClass("quoteDetailsFields");

		var btnRecentOrders= new sap.ui.commons.Button("recentOrders",{
								text : "Recent Orders",
								styled:false,
								press:function(event){
									hideNotifier();
                        			sap.ui.getCore().getEventBus().publish("nav", "back", {id : "LandingPage"});
                        		},
							}).addStyleClass("btnRecentOrders");
		oMatrix.createRow(customerPoNum, postingDate, btnRecentOrders);

		
						

var estimatedTotalPanel= new sap.ui.layout.HorizontalLayout({
					content:[ 
					new sap.ui.layout.HorizontalLayout({
						content:[
						new sap.m.Text({text:"Estimated Total"}).addStyleClass("estimatedTotalText"),
						new sap.m.Text("estimatedTotalValue",{text:"0"}).addStyleClass("estimatedTotalValue"),
						new sap.m.Text({text:"USD"}).addStyleClass("estimatedTotalUnit")
						]}).addStyleClass("estimatedTotalTextPanel")
						
						]		
		}).addStyleClass("estimatedTotalPanel");


//Create an instance of the product table control
		var productsTable = new sap.ui.table.TreeTable("productsTable", {
			title:"Quote Details",
			width: "750px",
			selectionBehavior: sap.ui.table.SelectionBehavior.RowOnly,
			columns: [
				
				new sap.ui.table.Column({
					label: new sap.ui.commons.Label({text: ""}),
					template: new sap.ui.commons.Button({
						text:"Configure",
						press: [oController.configureProduct, oController],
						visible:"{PROD_IS_CONFIGURABLE}"
					}).addStyleClass("btnConfigure"),
					width:"120px"
				}),
				new sap.ui.table.Column({
					label: new sap.ui.commons.Label({text: "Product ID"}),
					template: new sap.ui.commons.Label({text:"{PRODUCT_ID}"}),
					width:"170px"
				}),
				new sap.ui.table.Column({
					label: new sap.ui.commons.Label({text: "Product"}),
					template: new sap.ui.commons.Label({text:"{PRODUCT_DESC}"}),
					width: "260px"
				}),
				new sap.ui.table.Column({
					label: new sap.ui.commons.Label({text: "QTY"}),
					template: new sap.ui.commons.TextField({
						value:"{QUANTITY}",
						liveChange: [oController.quantityChangeHandler, oController],
						rowSelectionChange : [oController.cellClicked, oController]
					}),
					width: "48px"
				}),
				new sap.ui.table.Column({
					label:  new sap.ui.commons.Label({text: "Unit Cost($)"}),
					template: new sap.ui.commons.Label({text:"{UNIT_COST}"}).addStyleClass("costAlign")
				}),
				new sap.ui.table.Column({
					label:  new sap.ui.commons.Label({text: "Line Total($)"}),
					template: new sap.ui.commons.Label({text:"{LINE_TOTAL}"}).addStyleClass("costAlign")
				})
			],
			selectionMode: sap.ui.table.SelectionMode.Single,
			enableColumnReordering: true,
			expandFirstLevel: true,
			footer:estimatedTotalPanel
		});
		
		var productsModel = new sap.ui.model.json.JSONModel();
		// productsModel.loadData('model/configuredProducts.json',null,false);
		productsModel.setData(productsToBeAdded);
		productsTable.setModel(productsModel);
		productsTable.bindRows("/products");

		var buttonPanel=new sap.ui.layout.HorizontalLayout({
			content:[
				new sap.ui.commons.Button({
                        text: "Save as template",
                        styled:false
        		}).addStyleClass('btnSaveQuote'),

				new sap.ui.commons.Button({
                        text:"Complete Quote",
                        press: function(event){
                        	oController.submitQuote(event);
                        },
                        styled:false
        		}).addStyleClass('btnSubmitQuote')
			]
			
		}).addStyleClass("quoteBtnPanel");

		var disclaimerText= new sap.ui.layout.HorizontalLayout({ 
		content:[
				new sap.m.Text({text:"Quote cannot be completed until all configurable products have a configuration"}).addStyleClass("disclaimerText")
			]
		}).addStyleClass("disclaimerTextPanel");

		oController.productsTable=productsTable;
		oController.productsModel=productsModel;
		controls.push(mtitle);
		controls.push(oMatrix);
		//controls.push(btnRecentOrders);
		controls.push(productsTable);
		controls.push(buttonPanel);
		controls.push(disclaimerText);
		return controls;
	}

});