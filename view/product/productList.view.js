sap.ui.jsview("view.product.productList", {
	getControllerName : function() {
		return "view.product.productList";
	},
	createContent : function(oController) {
		var controls = [];
		var categories= new sap.m.HBox("prodSearchTabBar", {
					items:[ new sap.ui.commons.Button({
											  text: "Products",
											  styled: false
											  /*layoutData : new sap.m.FlexItemData({
															styleClass : "parentClass"
												})*/
							}).addStyleClass("btnProducts"),
							new sap.ui.commons.Button({text: "Favorites", styled:false,
								icon:"sap-icon://favorite"}).addStyleClass("btnFavourites")
					]
		}).addStyleClass("productListButtonPanel");

		

		var productListItemDetail = new sap.m.VBox({
			items :[
				new sap.m.HBox({
					items :[ new sap.m.VBox({ items:[new sap.m.Text({ text: "{ProductDesc}"}).addStyleClass("productName"),
							 new sap.m.Text({ text: "{ProductId}"}).addStyleClass("productDesc")]}),
							 new sap.m.Button({ icon: "sap-icon://favorite"}).addStyleClass("btnFavorites")]
			}).addStyleClass("productTitleIcon"),
				new sap.m.HBox({
					items:[ 
							new sap.ui.commons.Button({ text:"+ Add", 
														styled:false, 
														layoutData : new sap.m.FlexItemData({styleClass : "parentClassbtnAdd"
														}),
														press: function(event){
															oController.addProducts(event);
														}
													}).addStyleClass("btnAddProducts"),
							new sap.m.Text({ text: "{ProdIsConfigurable}", visible:false
								})
					]
				}),
				new sap.m.HBox("configurableBox",{
					items:[ 
							new sap.ui.core.Icon( {  
							src : sap.ui.core.IconPool.getIconURI( "fa-info-circle", "awesomeFonts"),
							decorative: false,
							densityAware: false,
							visible:{
								parts : [{path:"ProdIsConfigurable"}],					
								formatter:function(isConfig){
									if(isConfig==='X')return true;
									else return false;
									}
								}
							}).addStyleClass("configurableIcon"),
							
							new sap.m.Text({text:"Configurable",
								visible:{
								parts : [{path:"ProdIsConfigurable"}],					
								formatter:function(isConfig){
									if(isConfig==='X')return true;
									else return false;
									}
								}
							}).addStyleClass("configurableText")
						]
				}).addStyleClass("configurablePanel")
			]
		}).addStyleClass("productListItemDetail");

		var productItems=new sap.m.CustomListItem("productListItem",{ 
						content : productListItemDetail,
						type: sap.m.ListType.Active
			
		}).addStyleClass("productItems");
		

		oController.productList = new sap.m.List({items: []}).addStyleClass("productList");

		var lModel = new sap.ui.model.json.JSONModel('model/productDetails.json');
		//lModel.loadData('model/productDetails.json');
		sap.ui.getCore().setModel(lModel); 
		oController.productList.setModel(lModel); 
		oController.productList.bindItems("/products", productItems);

		

	    var productSearch =new sap.m.SearchField("productSearchField",{
	    	placeholder: "Search",
	    	search: function(event){
				oController.searchProduct(event, this);
			}
	    }).addStyleClass("productSearch");

	    oController.totalProducts= new sap.m.Text({}).addStyleClass("totalProductsReturned");
		
		var listContainer= new sap.m.VBox({
			items : [categories,productSearch,oController.totalProducts,oController.productList]
		}).addStyleClass("listContainer");
		
		controls.push(listContainer);
		return controls;
	}
});