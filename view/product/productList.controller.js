sap.ui.controller("view.product.productList", {
/*	onInit: function(){
		//this.applicationModelStack=[];
	},*/
	key:0,
	randomPrices:[235.45, 345.67, 480.25],
	onAfterRendering: function(){
		var searchValue="";
		this.getProductList(searchValue, false);
	
	},

	searchProduct: function(event,obj){
		var searchValue=obj.getValue();
		this.getProductList(searchValue, true);
	},

	getProductList:function(searchValue, showBusy){
		var gatewayUrl =oDataServices.searchProducts;
		var controller = this;
		var relPath = "ProductSearchReqSet('" + searchValue + "')/NAV_PROD_DETAILS/";				
		var urlArgument = "";
		if(showBusy){
			busyIndicatorMsg.busySpinner("Product Search","Searching Products...");
		}
			
		utilModel.makeGatewayServiceReadCall(gatewayUrl,relPath,urlArgument, controller.productListResponse,controller);
	},

	productListResponse:function(response,isSuccess){
		console.log(response);
		if(!isSuccess){
			notifier.showErrorMessage("Product search has failed.");
			busyDialog.close();
			setTimeout(function(){
			  notifier.hideNotifier();
			}, 10000);
			return;
		}
		busyDialog.close();
		var controller = this;
		var productsModelArr = response.results;
		controller.productList.getModel().setData({products : productsModelArr});

		var noOfProducts=productsModelArr.length;
		controller.totalProducts.setText("Returning " +noOfProducts+ " results");
	},

	addProducts:function(event,obj){
		var controller=this;
		var productId =event.getSource().getParent().getParent().mAggregations.items[0].mAggregations.items[0].mAggregations.items[1].mProperties.text;
		var productDesc =event.getSource().getParent().getParent().mAggregations.items[0].mAggregations.items[0].mAggregations.items[0].mProperties.text;
		var prodConfig=event.getSource().getParent().mAggregations.items[1].mProperties.text;
		var isConfig;
		if(prodConfig==="") isConfig=false;
		else if((prodConfig=="X")) isConfig=true;
		var productItem={
			"PRODUCT_ID": productId,
			"PRODUCT_DESC": productDesc,
			"QUANTITY": "1.0",
			"UNIT_COST": String(controller.randomPrices[controller.key]),
			"PROD_IS_CONFIGURABLE":isConfig,
			"LINE_TOTAL": String(controller.randomPrices[controller.key])
			
		};
		var estimatedTotal = 0;
		var rows = sap.ui.getCore().byId("productsTable").getRows();
			for(var i=0; i< rows.length; i++){
				var row = rows[i];
				var lineTotal = row.mAggregations.cells[5].getText();
				if(lineTotal === ""){
					break;
				}
					estimatedTotal += parseFloat(lineTotal);
				
			}
			estimatedTotal += parseFloat(controller.randomPrices[controller.key]);

			var formattedEstimatedTotal=estimatedTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
			sap.ui.getCore().byId("estimatedTotalValue").setText(formattedEstimatedTotal);

		/*estimatedTotal=parseFloat(estimatedTotal)+parseFloat(controller.randomPrices[controller.key]);
		var formattedEstimatedTotal=estimatedTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
		sap.ui.getCore().byId("estimatedTotalValue").setText(formattedEstimatedTotal);*/
		productsToBeAdded.products[String(controller.key)]=productItem;
		sap.ui.getCore().byId("productsTable").getModel().setData(productsToBeAdded);
		controller.key++;
		console.log(productsToBeAdded);
	}
		
});