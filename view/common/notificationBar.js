jQuery.sap.declare("lexmark.cpq.poc.view.common.notificationBar");

	/*
	 * Creating the message notifier with some messages
	 */


	var notifier={

	showNotifier: function(){
		oNotiBar3.fireDisplay({show:true});
	},

	hideNotifier :function(){
		oMessageNotifier.removeAllMessages();
		oNotiBar3.fireDisplay({show:false});
	},

	displayListener: function(oEvent) {
		var bShow = oEvent.getParameter("show");
		var sStatus;
		if (bShow) {
			/*
			 * Now the application can decide how to display the bar. It can be maximized, default, minimized (please see NotificationBarStatus) 
			 */
			sStatus = sap.ui.ux3.NotificationBarStatus.Default;
			oNotiBar3.setVisibleStatus(sStatus);
		} else {
			sStatus = sap.ui.ux3.NotificationBarStatus.None;
			oNotiBar3.setVisibleStatus(sStatus);
		}
	},

	 showSuccessMessage: function(message){
		var oMessage = new sap.ui.core.Message({
			text : message
		});
		oMessage.setLevel(sap.ui.core.MessageType.Success);
		oMessageNotifier.addMessage(oMessage);
		notifier.showNotifier();
	},


	 showErrorMessage: function(message){
		var oMessage = new sap.ui.core.Message({
			text : message
		});
		oMessage.setLevel(sap.ui.core.MessageType.Error);
		oMessageNotifier.addMessage(oMessage);
		notifier.showNotifier();
	}


};

	oMessageNotifier = new sap.ui.ux3.Notifier({
		title : "This is the MessageNotifier"
	});

	
	oNotiBar3 = new sap.ui.ux3.NotificationBar({
		display : notifier.displayListener,
		visibleStatus : "None"
	});
	oNotiBar3.addStyleClass("sapUiNotificationBarDemokit");
	oNotiBar3.setMessageNotifier(oMessageNotifier);
	
	