(function() {
	"use strict";
	var rootInstance;

	sap.ui.controller("view.product-configure.productConfigure", {
		key:0,
		randomPrices:[235.45, 345.67, 480.25, 123.78, 140.86],
		configureRootProduct : function() {
			var configureView = this.getView().configurepage;
			var lmodel = this.getView().getModel();
			rootInstance = lmodel.getData();
			var productConfigureTree = this.configureProduct(rootInstance,configureView,"rootPanel",true);
			
			productConfigureTree.setExpandable(false);
		},

		configureProduct:function(rootInstance, element, styleClass, expanded){
				
			var headerToolbar = new sap.m.Toolbar("ToolBar" + rootInstance.InstanceId,{
					content:[
						new sap.m.HBox({
							items:[
								new sap.m.Label({text:rootInstance.ProductDesc + "(" + rootInstance.InstanceId + ")"}),
								new sap.m.Label({text:"Quantity:" + rootInstance.Quantity}).addStyleClass('qty')
							]
						}),
						
						new sap.m.HBox("configComplete" + rootInstance.InstanceId,{
							items:[ 
									new sap.ui.core.Icon("icon" + rootInstance.InstanceId,  {  
									src : sap.ui.core.IconPool.getIconURI( "fa-info-circle", "awesomeFonts"),
									decorative: false,
									densityAware: false,
									}).addStyleClass("configIncompleteIcon"),
									
									new sap.m.Text("text" + rootInstance.InstanceId, {text:"Not Complete",
									}).addStyleClass("configIncompleteText")
								]
						})
					]
				}).addStyleClass("headerToolbar");
			var productConfigureTree = new sap.m.Panel("Panel" + rootInstance.InstanceId,{
				expandable : true,
				expanded : expanded,
				headerToolbar :  headerToolbar
			});
			productConfigureTree.addStyleClass(styleClass);

			var characteristics = rootInstance.NAV_HEADER_TO_ITEM_NAME.results;
			if(characteristics && characteristics.length > 0){
				var oMatrix = new sap.ui.commons.layout.MatrixLayout({layoutFixed: true, width:'740px', columns: 5});
				oMatrix.setWidths('120px', '200px', '100px', '120px', '200px');

				for(var j=0; j< characteristics.length; j++){
					
					var characteristic = characteristics[j];
					var firstColumnfield = this.createField(characteristic);

					if(j+1 < characteristics.length){
						j++;
						var blankLabel = new sap.m.Label({text: ""});
						characteristic = characteristics[j];
						var secondColumnfield = this.createField(characteristic);

						var someLabel = new   sap.ui.commons.Label( {text: ""});

						oMatrix.createRow(firstColumnfield[0], firstColumnfield[1], someLabel, secondColumnfield[0], secondColumnfield[1]);
					}else{
						oMatrix.createRow(firstColumnfield[0], firstColumnfield[1]);
					}
					productConfigureTree.addContent(oMatrix);
				}
			}

			element.addContent(productConfigureTree);
			return productConfigureTree;

		},

		createField:function(characteristic){
			var oLabel = new sap.m.Label({text: characteristic.CharLabel});
			oLabel.setRequired(characteristic.IsRequired === "T"?true:false);
		
			var oInput;
			var characteristicModel = new sap.ui.model.json.JSONModel(characteristic);
			if(characteristic.Control === "D"){
				oInput = this.createComboBox(characteristic);
			}else{
				oInput = this.createTextInput(characteristic);
			}

			oInput.setModel(characteristicModel);
			var allowedvalues = characteristic.NAV_ITEM_NAME_TO_VALUE.results;
			if(allowedvalues && allowedvalues.length > 0){
				if(allowedvalues.length === 1){
					if(allowedvalues[0].IsDefault === "T" || allowedvalues[0].IsValue === "T"){
						oInput.setValue(allowedvalues[0].CharValue);
						characteristic.valueset = allowedvalues[0].CharValue;
					}
				}else{

					this.setUpComboBox(characteristic, oInput);
				}
			}
			oLabel.setLabelFor(oInput);
			return [oLabel,oInput];
		},

		createComboBox:function(characteristic){
				var oItemTemplate = new sap.ui.core.Item({  
										  key : "{CharValue}",  
										  text : "{CharValueLabel}"  
										});  
				return new sap.m.ComboBox(characteristic.CharName + "_" + characteristic.InstanceId, {
					items:{
						path : "/NAV_ITEM_NAME_TO_VALUE/results",
						template : oItemTemplate  
					},
					change: [this.handleChange, this]	
				});
		},


		setUpComboBox:function(characteristic, oInput){
			var allowedvalues = characteristic.NAV_ITEM_NAME_TO_VALUE.results;
			for(var i=0; i< allowedvalues.length; i++){

						var allowedValue = allowedvalues[i];
						if(allowedValue.IsValue === "T"){
							oInput.setSelectedKey(allowedValue.CharValue);
							break;
						}
						if(allowedValue.IsDefault ===  "T"){
							oInput.setSelectedKey(allowedValue.CharValue);
						}	
					}
		},

		createTextInput:function(characteristic){

			return new sap.m.Input(characteristic.CharName+ "_" + characteristic.InstanceId, {
					value: "",
					width: '100%',
					change: [this.handleChange, this]
				});
		},

		handleChange: function(oEvent){
			var csticData = oEvent.getSource().getModel().getData();
	        var rootModel = this.getView().getModel();
	        var instance = rootModel.getData();

	        if(instance.InstanceId !== csticData.InstanceId){
	        	instance = this.findInstance(instance.instances, csticData);
	        }
	        
	        var characteristics = instance.NAV_HEADER_TO_ITEM_NAME.results;
			for(var j=0; j<characteristics.length; j++){
				var characteristic = characteristics[j];
				var characterName =  oEvent.getSource().getId().substring(0, oEvent.getSource().getId().lastIndexOf('_'));
				if(characteristic.CharName === characterName){
					this.setCharacteristicValue(characteristic, oEvent.getSource());
					break;
				}
			}
			if(this.isInstanceComplete(instance)){
				var parentProduct = sap.ui.getCore().byId("Panel" + instance.InstanceId);

				this.getChildrenInstances(instance);
			}
	    },

	    findInstance:function(instances,  csticData){
	    	if(instances && instances.length > 0){
	        	for(var i=0; i<instances.length; i++){
	        		if(instances[i].InstanceId === csticData.InstanceId){
	        			return instances[i];
	        		}
	        		else{
	        			this.findInstance(instances[i].instances, csticData);
	        		}	        		
	        	}
	        }
	    },

	    setCharacteristicValue:function(characteristic, source){
	    	if(characteristic.Control === "D"){
				var allowedvalues = characteristic.NAV_ITEM_NAME_TO_VALUE.results;
				for(var j=0; j<allowedvalues.length>0; j++){
					if(allowedvalues[j].CharValue === source.getSelectedKey()){
						allowedvalues[j].IsValue = "T";
					}else{
						allowedvalues[j].IsValue = "F";
					}
				}
				
			}else{
				characteristic.valueset = source.getValue();
			}
	    },

	    isInstanceComplete:function(instance){
	    	var characteristicCompleteCount = 0;
	    	var characteristics = instance.NAV_HEADER_TO_ITEM_NAME.results;
	    	for(var j=0; j<characteristics.length; j++){
	    		var characteristic = characteristics[j];
				if(this.isCharacteristicComplete(characteristic)){
					characteristicCompleteCount ++;
				}else{
					this.setInstanceCompleteness(instance, false);
					return false;
				}
			}
			if(characteristicCompleteCount === characteristics.length){
				this.setInstanceCompleteness(instance, true);
				return true;
			}
			
			return false;
	    },

	    setInstanceCompleteness:function(instance, isComplete){
	    	var instancesToSet = [];
	    	var allchildren = [];
	    	instance.isComplete = isComplete;
	    	var lmodel = this.getView().getModel();
			var rootInstance = lmodel.getData();
	    	if(rootInstance.instances){
	    		for(var i=0; i<rootInstance.instances.length; i++){
		    		if(rootInstance.instances[i].isComplete){
		    			allchildren.push(rootInstance.instances[i]);
		    		}
		    		instancesToSet.push(rootInstance.instances[i]);
		    	}

		    	if(allchildren.length == rootInstance.instances.length){
		    		rootInstance.isComplete = true;
		    		instancesToSet.push(rootInstance);
		    	}
		    	else{
		    		rootInstance.isComplete = false;
		    		instancesToSet.push(rootInstance);
		    	}

	    	}else{
	    		instancesToSet.push(rootInstance);
	    	}
	    	
	    	for(var j=0; j<instancesToSet.length; j++){
	    		var configCompleteIcon = sap.ui.getCore().byId("icon" + instancesToSet[j].InstanceId);
		    	var configCompleteText = sap.ui.getCore().byId("text" + instancesToSet[j].InstanceId);
		    	if(configCompleteIcon && configCompleteText){
		    		if(instancesToSet[j].isComplete){
		    		configCompleteIcon.removeStyleClass("configIncompleteIcon");
		    		configCompleteIcon.addStyleClass("configCompleteIcon");
		    		configCompleteText.setText("Complete");
			    	}else{
			    		configCompleteIcon.removeStyleClass("configCompleteIcon");
			    		configCompleteIcon.addStyleClass("configIncompleteIcon");
			    		configCompleteText.setText("Not Complete");
			    	}
		    	}
		    	
	    	}    	

	    },

	    getInstacnes:function(instance){
	    	var instances = [];
	    	instances.push(rootInstance.InstanceId);
	    	if(instance.InstanceId !== rootInstance.InstanceId){
	    		this.checkInstanceRecursively(instance, rootInstance.instances, instances);
	    	}    	

	    	return instances;
	    },

	    checkInstanceRecursively:function(instance, instancesToChcek, instances){
	    	if(instancesToChcek && instancesToChcek.length > 0){
	    		for(var i=0; i<instancesToChcek.length; i++){
	    			if(instancesToChcek[i].InstanceId === instance.InstanceId){
	    				instances.push(instancesToChcek[i].InstanceId);
	    				return instancesToChcek[i].ParentInstanceId;
	    			}else{
	    				var parentInstanceId = this.checkInstanceRecursively(instance, instancesToChcek[i].instances, instances);
	    				if(parentInstanceId){
	    					instances.push(instancesToChcek[i].InstanceId);
	    					return instancesToChcek[i].ParentInstanceId;
	    				}
	    			}
	    		}
	    	}
	    	return;
	    },

		isCharacteristicComplete:function(characteristic){
	    	if(characteristic.Control === "D"){
					var allowedvalues = characteristic.NAV_ITEM_NAME_TO_VALUE.results;
					for(var j=0; j<allowedvalues.length; j++){
						if(allowedvalues[j].IsValue === "T"){
							return true;
						}
					}
				}else if(characteristic.valueset && characteristic.valueset !== ''){
					return true;
				}
				return false;
	    },

		addInstances:function(response, status, instance){
			busyDialog.close();
			if(status){
				var rootModel = this.getView().getModel();
				var rootInstance = rootModel.getData();
		
				instance.instances = response.results;
			
				//Figure out which Instance is being configured
				var parentProduct = sap.ui.getCore().byId("Panel" + instance.InstanceId);
				//parentProduct.destroyContent();
				
				var instances = instance.instances;
				if(instances && instances.length > 0){
					for(var i=0; i<instances.length; i++){
						this.configureProduct(instances[i], parentProduct, "childPanel", false);
						this.isInstanceComplete(instances[i]);
					}
				}
			}
			
			else{
				notifier.showErrorMessage("Fetching sub-products has failed.");
				setTimeout(function(){
			  		notifier.hideNotifier();
				}, 10000);
				return;

			}
			
		},

		getChildrenInstances:function(instance){
			var url = oDataServices.setCharacteristics;
	
			var initialRelPath = "ChildProdCsticsReqSet(QuotationId='QUOTEID',SessionId='SESSION-ID',InstanceId='INSTANCE-ID',";
			initialRelPath = initialRelPath.replace('QUOTEID', appConfig.quoteId);
			initialRelPath = initialRelPath.replace('SESSION-ID', appConfig.sessionIds[rootInstance	.InstanceId]);
			initialRelPath = initialRelPath.replace('INSTANCE-ID', instance.InstanceId);

			var characteristics = instance.NAV_HEADER_TO_ITEM_NAME.results;
			var charNamesAndValues = "";
	    	for(var j=0; j<characteristics.length; j++){
	    		var characteristic = characteristics[j];
	    		charNamesAndValues += "Cname" + (j+1) + "='" + characteristic.CharName + "',Cvalue" + (j+1) + "='" + this.getCharacteristicValue(characteristic) + "',";
	    	}
			

			var finalRelPath = initialRelPath + charNamesAndValues + "ItemIndex='" + (appConfig.currentProductIndex +1) + "')/NAV_REQ_TO_RES/?$expand=NAV_HEADER_TO_ITEM_NAME/NAV_ITEM_NAME_TO_VALUE";
			busyIndicatorMsg.busySpinner("Fetch Children Products","Fetching children...");
			utilModel.makeGatewayServiceReadCall(url, finalRelPath, '', this.addInstances, this, instance);
		},

		getCharacteristicValue:function(characteristic){
			if(characteristic.Control === "D"){
					var allowedvalues = characteristic.NAV_ITEM_NAME_TO_VALUE.results;
					for(var j=0; j<allowedvalues.length; j++){
						if(allowedvalues[j].IsValue === "T"){
							return allowedvalues[j].CharValue;
						}
					}
				}else if(characteristic.valueset && characteristic.valueset !== ''){
					return characteristic.valueset;
				}
		},

		saveConfiguration:function(){
			var productsTable = sap.ui.getCore().byId("productsTable");
			var productsModelData = productsTable.getModel().getData().products;
			var lmodel = this.getView().getModel();
			var rootInstance = lmodel.getData();
			for(var propName in productsModelData){
				var selectedIndex = String(appConfig.currentProductIndex);
				if(propName === selectedIndex){
					if(productsModelData[propName].PRODUCT_ID === rootInstance.ProductId){
						var childrenInstances = rootInstance.instances;
						for(var i = 0;i< childrenInstances.length; i++){
							var childProduct = this.prepareChildInstance(childrenInstances[i]);
							productsModelData[propName][String(i)] = childProduct;
						}
					}
					break;
				}
			}

			//Reset all settings
			appConfig.currentProductIndex = -1;
			//delete appConfig.sessionIds[rootInstance.InstanceId];
			productsTable.setSelectedIndex(-1);
			productsTable.expand(0);

			setTimeout(function(){
			  		var estimatedTotal = 0;
					var rows = sap.ui.getCore().byId("productsTable").getRows();
					for(var i=0; i< rows.length; i++){
						var row = rows[i];
						var lineTotal = row.mAggregations.cells[5].getText();
						if(lineTotal === ""){
							break;
						}
							estimatedTotal += parseFloat(lineTotal);
						
					}

					var formattedEstimatedTotal=estimatedTotal.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,');
					sap.ui.getCore().byId("estimatedTotalValue").setText(formattedEstimatedTotal);
				}, 100);

			
			
		},

		prepareChildInstance:function(configuredInstance){
			var instance = {};
			instance.PRODUCT_ID = configuredInstance.ProductId;
			instance.PRODUCT_DESC = configuredInstance.ProductDesc;
			instance.QUANTITY = configuredInstance.Quantity;
			instance.UNIT_COST = String(this.randomPrices[this.key]);
			instance.LINE_TOTAL = String(this.randomPrices[this.key]);
			instance.PROD_IS_CONFIGURABLE = false;
			if(configuredInstance.instances && configuredInstance.instances.length > 0){
				var childrenInstances = configuredInstance.instances;
				for(var i = 0;i< childrenInstances.length; i++){
					var childProduct = this.prepareChildInstance(childrenInstances[i]);
					instance[String(i)] = childProduct;
				}
			}
			this.key++;
			return instance;
		}
	});	
})();