sap.ui.jsview("view.product-configure.productConfigure", {
	getControllerName : function() {
		return "view.product-configure.productConfigure";
	},

	
	createContent : function(oController) {
		var controls = [];
		this.addStyleClass("prodConfigure");
		var oBtnCancel = new sap.ui.commons.Button("cancelBtn", {
				text : "Cancel",
				press : function() {
					oController.getView().configurepage.destroyContent();
					sap.ui.getCore().getEventBus().publish("nav", "back", {id : "CreateQuote"});
				}

			});

		var oBtnSaveConfig = new sap.ui.commons.Button("saveConfigBtn",{
			text : "Save Configuration",
			press : function() {
				oController.saveConfiguration();
				oController.getView().configurepage.destroyContent();
				sap.ui.getCore().getEventBus().publish("nav", "back", {id : "CreateQuote"});
			}
		});

		var fixFlex = new sap.m.FlexBox({
			direction: sap.m.FlexDirection.RowReverse,
			items:[oBtnSaveConfig,oBtnCancel],
			width:"98%",
			alignItems:sap.m.FlexAlignItems.Start});

		this.configurepage = new sap.m.Page("configurepage",{
			showHeader:false,
			footer: new sap.m.Toolbar("prodToolBar",{
				content:[fixFlex]
			})

		});

		controls.push(this.configurepage);
		

		return controls;
	}

});