jQuery.sap.declare("lexmark.cpq.poc.view.general.busyIndicator");
var busyIndicator = new sap.m.BusyIndicator("busyIndicator");
var busyDialog = new sap.ui.commons.Dialog("busyDialog",{
						modal : true,
						width:"350px",
						height:"170px",
						showCloseButton:false,
						content : [ busyIndicator ]
					}).addStyleClass("busyDialog");

var busyIndicatorMsg ={
	busySpinner:function(title,msg){
		busyDialog.open();
		sap.ui.getCore().byId("busyIndicator").setText(msg);
		sap.ui.getCore().byId("busyDialog").setTitle(title);
	}
};