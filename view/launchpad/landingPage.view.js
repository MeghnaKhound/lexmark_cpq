sap.ui.jsview("view.launchpad.landingPage", {

    getControllerName: function() {
        return "view.launchpad.landingPage";
    },

    createContent: function(oController) {

        var tc = new sap.m.TileContainer("tc", {});

        var model = new sap.ui.model.json.JSONModel("model/menu.json");
        model.attachRequestCompleted(null, function() {
            function navFn(target) {
                return function() {
                    oController.doNavOnSelect(target);
                };
            }

            var data = null,
                m = 0,
                menu = null;
            data = model.getData();
            if (data && data.Menu) {
                for (m = 0; m < data.Menu.length; m++) {
                    menu = data.Menu[m];
                    tc.addTile(new sap.m.StandardTile({
                        icon: menu.icon,
                        title: menu.title,
                        info: menu.description,
                        width:"150px",
                        height:"200px",
                        press: navFn(menu.targetPage)
                    }));
                }
            }

        });

        var page = new sap.m.Page({
            showHeader: true,
            
        });

        page.setEnableScrolling(false);
        page.setShowHeader(false);
        page.addContent(tc);

        return page;
    }

});