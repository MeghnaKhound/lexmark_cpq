//UI5 Boilerplate Configuration
jQuery.sap.declare("lexmark.cpq.poc.util.config");

appConfig = {
	quoteId : "",
	sessionIds:{},
	currentProductIndex:-1

};

lexmarkFonts = {

	addFontToPool : function (fontName, collnName, fontClass, content) {

		jQuery.sap.require("sap.ui.core.IconPool");

		sap.ui.core.IconPool.addIcon(fontName,collnName,fontClass,content);
	},

	addKnownFontsToPool : function () {

		this.addFontToPool("fa-home","awesomeFonts","FontAwesome",'f015');
		this.addFontToPool("fa-table","awesomeFonts","FontAwesome",'f0ce');
		this.addFontToPool("fa-bar-chart","awesomeFonts","FontAwesome",'f080');
		this.addFontToPool("fa-slider","awesomeFonts","FontAwesome",'f1de');
		this.addFontToPool("fa-bell","awesomeFonts","FontAwesome",'f0f3');
		this.addFontToPool("fa-tag","awesomeFonts","FontAwesome",'f02b');
		this.addFontToPool("fa-info-circle","awesomeFonts","FontAwesome",'f111');
		this.addFontToPool("fa fa-circle","awesomeFonts","FontAwesome",'f05a');
		this.addFontToPool("fa-money","awesomeFonts","FontAwesome",'f0d6');
		this.addFontToPool("fa-binoculars","awesomeFonts","FontAwesome",'f1e5');
		this.addFontToPool("fa-cog","awesomeFonts","FontAwesome",'f013');
		this.addFontToPool("fa-square-o","awesomeFonts","FontAwesome",'f096');
		this.addFontToPool("fa-check-square-o","awesomeFonts","FontAwesome",'f046');
		this.addFontToPool("fa-circle-o","awesomeFonts","FontAwesome",'f10c');
		this.addFontToPool("fa-dot-circle-o","awesomeFonts","FontAwesome",'f192');
		this.addFontToPool("fa-plus-square-o","awesomeFonts","FontAwesome",'f196');
		this.addFontToPool("icon-plus","awesomeFonts","FontAwesome",'f067');
		this.addFontToPool("fa-minus-square-o","awesomeFonts","FontAwesome",'f147');
		this.addFontToPool("fa-minus","awesomeFonts","FontAwesome",'f068');
		this.addFontToPool("fa-sort-asc","awesomeFonts","FontAwesome",'f0de');
		this.addFontToPool("fa-sort-desc","awesomeFonts","FontAwesome",'f0dd');
		this.addFontToPool("fa-tachometer","awesomeFonts","FontAwesome",'f0e4');
		this.addFontToPool("fa-users","awesomeFonts","FontAwesome",'f0c0');
		this.addFontToPool("fa-download","awesomeFonts","FontAwesome",'f019');
		this.addFontToPool("fa-trash","awesomeFonts","FontAwesome",'f1f8');
		this.addFontToPool("fa-usd","awesomeFonts","FontAwesome",'f155');
		this.addFontToPool("fa-paper-plane-o","awesomeFonts","FontAwesome",'f1d9');
		this.addFontToPool("fa-check","awesomeFonts","FontAwesome",'f00c');
		this.addFontToPool("fa-times","awesomeFonts","FontAwesome",'f00d');
		this.addFontToPool("icon-ok", "awesomeFonts", "FontAwesome", "f00c");
		this.addFontToPool("icon-remove", "awesomeFonts", "FontAwesome", "f00d");
		this.addFontToPool("fa-arrow-left", "awesomeFonts", "FontAwesome", "f104");
		this.addFontToPool("fa-arrow-right", "awesomeFonts", "FontAwesome", "f105");
		this.addFontToPool("fa-sign-out", "awesomeFonts", "FontAwesome", "f08b");
		this.addFontToPool("fa-caret-right", "awesomeFonts", "FontAwesome", "f0da");
		this.addFontToPool("fa-caret-down", "awesomeFonts", "FontAwesome", "f0d7");
		this.addFontToPool("fa-chevron-down", "awesomeFonts", "FontAwesome", "f078");
		this.addFontToPool("fa-list-alt", "awesomeFonts", "FontAwesome", "f022");
		this.addFontToPool("fa-search", "awesomeFonts", "FontAwesome", "f002");
	}
};
