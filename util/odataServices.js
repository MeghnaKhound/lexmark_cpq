jQuery.sap.declare("lexmark.cpq.poc.util.odataServices");
 
var oDataServices = {
	searchProducts : "/lexmark/sap/opu/odata/sap/zgw_product_search_srv/",
	createQuote : "/lexmark/sap/opu/odata/sap/ZGW_QUOTATION_CREATE_SRV",
	getCharacteristics : "/lexmark/sap/opu/odata/sap/ZGW_GET_CONFIG_PROD_CSTICS_SRV",
	setCharacteristics : "/lexmark/sap/opu/odata/sap/ZGW_GET_CONF_PROD_CHILD_CTICS_SRV",
	updateQuote : "/lexmark/sap/opu/odata/sap/ZGW_QUOTATION_UPDATE_SRV"
};